<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            
            'title' => '7 Makanan Tradisional Khas Jawa yang cocok dissajikan hut Ri ke-75',
            'content' => 'prayaan perayaan tertentu nasi kuning sangat cocok disajikan',
            'category' =>  'Hut RI Ke-75'

    
            
        ]);
    }
}
